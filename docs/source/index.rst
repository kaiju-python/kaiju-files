
.. raw:: html

   <hr>

.. include:: ../../README.rst
.. include:: overview.rst
.. include:: dev_guide.rst
.. include:: license.rst

Reference
---------

.. toctree::
  :maxdepth: 2

  modules
