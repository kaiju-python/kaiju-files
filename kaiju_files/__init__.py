from .types import *
from .services import *

__version__ = '2.1.2'
__python_version__ = '3.8'
__author__ = 'antonnidhoggr@me.com'
